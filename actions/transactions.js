const transaction = (transactionPayload) => ({
  type: 'TRANSACTION',
  transaction: transactionPayload
});

export const startTransaction = (transactionObj) => dispatch => {
  dispatch(transaction(transactionObj));
};