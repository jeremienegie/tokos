import moment from 'moment';

const thisWeek = (thisWeekArr) => ({
  type: 'NOTIFICATIONS_THIS_WEEK',
  thisWeek: thisWeekArr
});

const lastWeek = (lastWeekArr) => ({
  type: 'NOTIFICATIONS_LAST_WEEK',
  lastWeek: lastWeekArr
});

const older = (olderArr) => ({
  type: 'NOTIFICATIONS_OLDER',
  older: olderArr
});

export const startNotifications = () =>
  (dispatch, getState) => getState().transactions.map(notification => {
    if (moment(notification.sentAt).isSame(moment(), 'week')) {
      return dispatch(thisWeek(notification));
    } else if (moment(notification.sentAt).isSame(moment().subtract(7, 'd'), 'week')) {
      return dispatch(lastWeek(notification));
    }
    return dispatch(older(notification));
  });