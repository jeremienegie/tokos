export const walletTrans = (transaction) => ({
	type: 'WALLET_TRANSACTION',
	walletTrans: transaction
});