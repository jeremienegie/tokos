export const overlay = ({
  status
}) => ({
  type: 'OVERLAY',
  status
});

export const amount = (amountTyped = '0,00') => ({
  type: 'AMOUNT',
  amountTyped
});

export const preSend = ({
  name,
  phone,
  avatar
}) => ({
  type: 'PRE_SEND',
  data: {
    name,
    phone,
    avatar
  }
});

export const transactionChoice = (choice) => ({
  type: 'TRANSACTION_CHOICE',
  choice
});

export const setCurrency = (currency) => ({
  type: 'SET_CURRENCY',
  currency
});

export const visibleTransHistory = (user) => ({
  type: 'VISIBLE_TRANS_HISTORY',
  user
});

export const setContacts = (value, contacts) => ({
  type: 'SET_CONTACTS',
  value,
  contacts
});

export const showDeleteCardBtn = ({
  status
}) => ({
  type: 'SHOW_DELETE_CARD_BTN',
  status
});