export const thisWeek = (state = [], action) => {
  const thisWeekArr = [...state, action.thisWeek];
  switch (action.type) {
  case 'NOTIFICATIONS_THIS_WEEK':
    return [...new Set(thisWeekArr)];
  default:
    return state;
  }
};

export const lastWeek = (state = [], action) => {
  const lastWeekArr = [...state, action.lastWeek];
  switch (action.type) {
  case 'NOTIFICATIONS_LAST_WEEK':
    return [...new Set(lastWeekArr)];
  default:
    return state;
  }
};

export const older = (state = [], action) => {
  const olderArr = [...state, action.older];
  switch (action.type) {
  case 'NOTIFICATIONS_OLDER':
    return [...new Set(olderArr)];
  default:
    return state;
  }
};