const INITIAL_STATE = [];

export const transactions = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'TRANSACTION':
      return [
        ...state,
        action.transaction
      ];
    default:
      return state;
  }
};