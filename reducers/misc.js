const INTIAL_STATE = {
  status: false
};

export const overlay = (state = INTIAL_STATE, action) => {
  switch (action.type) {
  case 'OVERLAY':
    return {
      status: action.status
    };
  default:
    return state;
  }
};

export const amountTyped = (state = {
  amountTyped: '0,00'
}, action) => {
  switch (action.type) {
  case 'AMOUNT':
    return {
      amountTyped: action.amountTyped
    };
  default:
    return state;
  }
};

export const preSend = (state = {}, action) => {
  switch (action.type) {
  case 'PRE_SEND':
    return { ...action.data
    };
  default:
    return state;
  }
};

export const transactionChoice = (state = {
  choice: ''
}, action) => {
  switch (action.type) {
  case 'TRANSACTION_CHOICE':
    return {
      choice: action.choice
    };
  default:
    return state;
  }
};

export const setCurrency = (state = {
  currency: 'FC'
}, action) => {
  switch (action.type) {
  case 'SET_CURRENCY':
    return {
      currency: action.currency
    };
  default:
    return state;
  }
};

export const visibleTransHistory = (state = {}, action) => {
  switch (action.type) {
  case 'VISIBLE_TRANS_HISTORY':
    return action.user;
  default:
    return state;
  }
};

export const setContacts = (state = {
  value: '',
  contacts: []
}, action) => {
  switch (action.type) {
  case 'SET_CONTACTS':
    return {
      value: action.value,
      contacts: action.contacts
    };
  default:
    return state;
  }
};

export const showDeleteCardBtn = (state = {
  status: false
}, action) => {
  switch (action.type) {
  case 'SHOW_DELETE_CARD_BTN':
    return {
      status: action.status
    };
  default:
    return state;
  }
};