export const wallet = (state = {}, action) => {
  switch (action.type) {
    case 'WALLET_TRANSACTION':
      return {
        walletTrans: action.walletTrans
      };
    default:
      return state;
  }
};