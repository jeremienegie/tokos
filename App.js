import React from 'react';
import {
  View
}
from 'react-native';
import {
  createStackNavigator
}
from 'react-navigation';
import Expo, {
  Font,
  AppLoading
}
from 'expo';
import {
  Provider
}
from 'react-redux';
import FlashMessage from 'react-native-flash-message';

import configureStore from './store';

import Transaction from './screens/Transaction';
import Contacts from './screens/Contacts';
import QRScanner from './screens/QRScanner';
import QRCode from './screens/QRCode';
import TransactionHistory from './screens/TransactionHistory';
import AddBankCard from './screens/AddBankCard';
import MaterialBottomTabs from './components/MaterialBottomTabs';
import {
  setContacts
}
from './actions';

const font = require('./assets/fonts/icon-font.ttf');

const store = configureStore();

export default class App extends React.Component {
  state = {
    fontLoaded: false
  }

  componentWillMount = () => {
    this.handleContacts();
  }


  async componentDidMount() {
    await Font.loadAsync({
      'icon-font': font
    });

    this.setState({
      fontLoaded: true
    });
  }

  handleContacts = async () => {
    const {
      status
    } = await Expo.Permissions.askAsync(Expo.Permissions.CONTACTS);
    let contacts = await Expo.Contacts.getContactsAsync({
      fields: [Expo.Contacts.PHONE_NUMBERS],
      pageSize: 1000
    });

    contacts = [contacts];
    store.dispatch(setContacts(status, contacts));
  }

  render() {
    const MainNavigator = createStackNavigator({
      main: MaterialBottomTabs,
      QRCode: {
        screen: QRCode
      },
      transactionHistory: {
        screen: TransactionHistory
      },
      transaction: {
        screen: Transaction
      },
      AddBankCard: {
        screen: AddBankCard
      },
      QRScanner: {
        screen: QRScanner
      },
      contacts: {
        screen: props => <Contacts
        {...props}
        handleContacts={this.handleContacts}
        isAllowed={this.state.status}
        contacts={this.state.contacts}
        />
      },
    }, {
      navigationOptions: {
        tabBarVisible: false,
        header: null,
        showLabel: false
      },
      cardStyle: {
        backgroundColor: '#ffffff'
      }
    });
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          {
            this.state.fontLoaded
            ? <MainNavigator />
            : <AppLoading style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} />
          }
          <FlashMessage position="top" />
        </View>
      </Provider>
    );
  }
}