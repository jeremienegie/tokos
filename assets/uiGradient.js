export default [{
    colorOne: '#ffafbd',
    colorTwo: '#ffc3a0'
  },
  {
    colorOne: '#2193b0',
    colorTwo: '#6dd5ed'
  },
  {
    colorOne: '#cc2b5e',
    colorTwo: '#753a88'
  },
  {
    colorOne: '#ee9ca7',
    colorTwo: '#ffdde1'
  },
  {
    colorOne: '#42275a',
    colorTwo: '#734b6d'
  },
  {
    colorOne: '#bdc3c7',
    colorTwo: '#2c3e50'
  },
  {
    colorOne: '#de6262',
    colorTwo: '#ffb88c'
  },
  {
    colorOne: '#06beb6',
    colorTwo: '#48b1bf'
  },
  {
    colorOne: '#eb3349',
    colorTwo: '#f45c43'
  },
  {
    colorOne: '#dd5e89',
    colorTwo: '#f7bb97'
  },
  {
    colorOne: '#56ab2f',
    colorTwo: '#a8e063'
  },
  {
    colorOne: '#614385',
    colorTwo: '#516395'
  },
  {
    colorOne: '#eacda3',
    colorTwo: '#d6ae7b'
  },
  {
    colorOne: '#02aab0',
    colorTwo: '#00cdac'
  },
  {
    colorOne: '#d66d75',
    colorTwo: '#e29587'
  },
  {
    colorOne: '#000428',
    colorTwo: '#004e92'
  },
  {
    colorOne: '#ddd6f3',
    colorTwo: '#faaca8'
  },
  {
    colorOne: '#7b4397',
    colorTwo: '#dc2430'
  },
  {
    colorOne: '#43cea2',
    colorTwo: '#185a9d'
  },
  {
    colorOne: '#ff512f',
    colorTwo: '#dd2476'
  },
  {
    colorOne: '#4568dc',
    colorTwo: '#b06ab3'
  },
  {
    colorOne: '#ec6f66',
    colorTwo: '#f3a183'
  },
  {
    colorOne: '#4ca1af',
    colorTwo: '#c4e0e5'
  },
  {
    colorOne: '#ff5f6d',
    colorTwo: '#ffc371'
  },
  {
    colorOne: '#36d1dc',
    colorTwo: '#5b86e5'
  },
  {
    colorOne: '#c33764',
    colorTwo: '#1d2671'
  },
  {
    colorOne: '#141e30',
    colorTwo: '#243b55'
  },
  {
    colorOne: '#ff7e5f',
    colorTwo: '#feb47b'
  },
  {
    colorOne: '#2b5876',
    colorTwo: '#4e4376'
  },
  {
    colorOne: '#ff9966',
    colorTwo: '#ff5e62'
  },
  {
    colorOne: '#aa076b',
    colorTwo: '#61045f'
  },
  {
    colorOne: '#52E5E7',
    colorTwo: '#130CB7'
  },
  {
    colorOne: '#2AFADF',
    colorTwo: '#4C83FF'
  },
  {
    colorOne: '#97ABFF',
    colorTwo: '#123597'
  },
  {
    colorOne: '#EE9AE5',
    colorTwo: '#5961F9'
  }
];