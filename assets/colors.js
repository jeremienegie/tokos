export default {
  green: '#1EC635',
  lightGreen: '#16CC60',
  grey: '#d1d8e0',
  black: '#495057'
};