import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StyleSheet,
  Platform,
  StatusBar
}
from 'react-native';
import Keyboard from 'react-native-keyboard';
import numeral from 'numeral';
import {
  connect
}
from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';

import {
  amount,
  setCurrency
}
from '../actions';
import colors from '../assets/colors';
import BackButton from '../components/BackButton';

const models = {

  keys: [],

  listeners: [],

  addKey(key) {
    const keySplitArr = this.keys.slice(this.keys.indexOf('.'));
    keySplitArr.shift();
    const decimalLength = keySplitArr ? keySplitArr.length : 0;

    if (this.keys.length !== 12 && decimalLength !== 2) {
      this.keys.push(key);
      this.notify();
    }
  },

  delKey() {
    this.keys.pop();
    this.notify();
  },

  clearAll() {
    this.keys = [];
    this.notify();
  },
  getKeys() {
    return this.keys;
  },

  onChange(listener) {
    if (typeof listener === 'function') {
      this.listeners.push(listener);
    }
  },

  notify() {
    this.listeners.forEach((listner) => {
      listner(this);
    });
  }
};

class KeyboardComp extends Component {
  static navigationOptions = {
    header: null,
    swipeEnabled: false
  }

  componentWillMount = () => {
    models.keys = [];
    this.props.amount('0.00');
  }

  componentDidMount = () => {
    models.onChange(model => {
      this.props.amount(model.getKeys().join(''));
    });
  }

  handleClear = () => {
    models.clearAll();
  }

  handleDelete = () => {
    models.delKey();
  }

  handleKeyPress = (key) => {
    models.addKey(key);
  }

  render() {
    const {
      inputContainer,
      container,
      allContainer,
      textStyle,
      button
    } = styles;
    return (
      <View style={container}>
        <View style={allContainer}>
          <BackButton
            dispatch={this.props.dispatch}
            to={this.props.to}
          />
          <View style={inputContainer}>
            <View>
                <Text style={textStyle}>{numeral(this.props.amountTyped).format('0,0.00')}</Text>
            </View>
            <View>
              <ModalDropdown
                textStyle={{
                  fontSize: 30
                }}
                dropdownStyle={{
                  padding: 5
                }}
                dropdownTextStyle={{
                  fontSize: 30
                }}
                options={['FC', '$']}
                defaultValue={this.props.currency}
                showsVerticalScrollIndicator={false}
                onSelect={(index, value) => this.props.setCurrency(value)}
              />
            </View>
          </View>
          <View style={button}>
            {this.props.children}
          </View>
          <Keyboard
              keyboardType="decimal-pad"
              onClear={this.handleClear}
              onDelete={this.handleDelete}
              onKeyPress={this.handleKeyPress}
              disableOtherText
              disableBorder
              disableClearButtonBackground
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
  },
  allContainer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 20,
    padding: 30
  },
  textStyle: {
    fontSize: 30
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.green,
    borderRadius: 5,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 20,
    marginHorizontal: 20
  },
});

const mapStateToProps = (state) => ({
  amountTyped: state.amountTyped.amountTyped,
  currency: state.setCurrency.currency
});

export default connect(mapStateToProps, {
  amount,
  setCurrency
})(KeyboardComp);