import React, {
  Component
}
from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet
}
from 'react-native';
import {
  connect
}
from 'react-redux';
import {
  showMessage
}
from 'react-native-flash-message';

import {
  transactionChoice
}
from '../actions';
import Keyboard from './Keyboard';
import Divider from './Divider';

class MainTransKeyboard extends Component {
  handleButtonClick = (choice) => {
    if (this.toLowToSend()) {
      return showMessage({
        message: `Sorry, you can not ${choice.toLowerCase()} ${this.props.amount} ${this.props.currency}`,
        type: 'danger',
        icon: 'auto',
        hideStatusBar: true,
        duration: 3000
      });
    }

    this.props.transactionChoice(choice);
    this.props.navigate('contacts');
  }
  toLowToSend = () => {
    if (parseInt(this.props.amount, 10) >= 100 && this.props.currency === 'FC') {
      return false;
    } else if (parseInt(this.props.amount, 10) >= 1 && this.props.currency === '$') {
      return false;
    }
    return true;
  }
  render() {
    const {
      buttonSelf,
      buttonText
    } = styles;
    return (
      <Keyboard
        dispatch={this.props.dispatch}
        to="home"
      >
          <TouchableOpacity
            style={buttonSelf}
            onPress={() => this.handleButtonClick('Envoyer')}
          >
            <Text style={buttonText}>&#xe904; Envoyer</Text>
          </TouchableOpacity>
          <Divider color="#ffffff" />
          <TouchableOpacity
            style={buttonSelf}
            onPress={() => this.handleButtonClick('Demander')}
          >
            <Text style={buttonText}>&#xe901; Demander</Text>
          </TouchableOpacity>
          <Divider color="#ffffff" />
          <TouchableOpacity
            style={buttonSelf}
            onPress={() => this.props.navigate('QRScanner')}
          >
            <Text style={buttonText}>&#xe907; Scanner</Text>
          </TouchableOpacity>
        </Keyboard>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    color: '#ffffff',
    fontFamily: 'icon-font',
    fontSize: 16
  },
  buttonSelf: {
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    justifyContent: 'center',
    height: '100%',
    paddingHorizontal: 10
  }
});

const mapStateToProps = (state) => ({
  amount: state.amountTyped.amountTyped,
  currency: state.setCurrency.currency
});

export default connect(mapStateToProps, {
  transactionChoice
})(MainTransKeyboard);