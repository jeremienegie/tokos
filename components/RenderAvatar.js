import React from 'react';
import {
  View
}
from 'react-native';
import {
  Avatar,
  Icon
}
from 'react-native-elements';

export default (props) => (
  <View>
    {
      props.avatar
      ? <Avatar
        size={props.avatarSize}
        source={{ uri: props.avatar }}
        size={props.size}
        rounded
      />
      : <Icon
        type="font-awesome"
        name="user-circle"
        size={props.size}
        color={props.color}
      />
    }
  </View>
);