import React, {
  Component
}
from 'react';
import {
  View,
  StyleSheet
}
from 'react-native';
import {
  Icon
}
from 'react-native-elements';
import {
  NavigationActions
}
from 'react-navigation';

export default class BackButton extends Component {
  handleBackClick = () => {
    const navigateAction = NavigationActions.back({
      routeName: this.props.to
    });
    this.props.dispatch(navigateAction);
  }
  render() {
    const {
      backButton
    } = styles;
    return (
      <View style={{ flex: 1 }}>
        <View style={[backButton, { ...this.props.customStyle }]}>
          <Icon
            name="arrow-left"
            color="#495057"
            type="font-awesome"
            onPress={() => this.handleBackClick()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backButton: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    position: 'absolute',
    top: 20,
    left: 20
  }
});