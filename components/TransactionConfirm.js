import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions
}
from 'react-native';
import {
  Overlay,
  Icon,
  Button,
  Input
}
from 'react-native-elements';
import numeral from 'numeral';
import moment from 'moment';
import {
  NavigationActions
}
from 'react-navigation';

import colors from '../assets/colors';
import RenderAvatar from '../components/RenderAvatar';

const {
  width
} = Dimensions.get('window');

export default class TransactionConfirm extends Component {
  state = {
    description: ''
  }
  handleTransaction = (transaction, receiverInfo) => {
    this.props.startTransaction(transaction);
    const visibleTransHistory = () => {
      const user = { ...receiverInfo,
        goBack: 'Home'
      };
      const navigateAction = NavigationActions.navigate({
        routeName: 'transactionHistory'
      });
      this.props.visibleTransHistory(user);
      this.props.dispatch(navigateAction);
    };
    this.props.overlayClose({
      status: false
    });
    this.props.amountReset();
    this.setState({
      description: ''
    });
    return visibleTransHistory();
  }
  render() {
    const {
      name,
      phone,
      amountToSend,
      avatar,
      choice,
      currency
    } = this.props.data;
    const receiverPhone = this.props.data.receiverPhone || phone;
    const transaction = {
      phone: receiverPhone,
      amount: amountToSend,
      choice,
      currency,
      description: this.state.description,
      sentAt: moment()
    };
    const receiverInfo = {
      avatar,
      name,
      receiverPhone
    };
    const {
      iconStyle,
      inputStyle,
      inputContainer,
      buttonStyle,
      innnerContainer
    } = styles;
    return (
      <Overlay
        isVisible={this.props.isVisible}
        fullScreen
        borderRadius={0}
      >
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-end'
        }}
      >
        <View style={iconStyle}>
          <Icon
            name="times"
            size={30}
            color="black"
            type="font-awesome"
            onPress={() => this.props.overlayClose({ status: false })}
          />
        </View>
        <View style={innnerContainer}>
        <View>
          <View style={{ marginBottom: 10 }}>
            <RenderAvatar
              avatarSize="large"
              size={80}
              color={colors.grey}
              avatar={avatar}
            />
          </View>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 16,
              fontWeight: 'bold',
              marginBottom: 5
            }}
          >
            {name}
          </Text>
          <Text style={{ textAlign: 'center', color: colors.grey }}>{receiverPhone}</Text>
        </View>
        <View style={inputContainer}>
          <Input
            placeholder="What is this for?"
            shake
            inputStyle={inputStyle}
            onChangeText={description => this.setState({ description })}
            inputContainerStyle={{
              borderBottomColor: 'rgba(0,0,0,0)',
              alignItems: 'center',
              justifyContent: 'center'
            }}
            underlineColorAndroid="transparent"
          />
        </View>
        <View>
          <Text style={{ fontSize: 32, fontWeight: 'bold' }}>{`${amountToSend} ${currency}`}</Text>
        </View>
        <View>
          <Text style={{ textAlign: 'center', marginBottom: 20 }}>Envoye avec Tokos</Text>
          <Button
            onPress={() => this.handleTransaction(transaction, receiverInfo)}
            buttonStyle={buttonStyle}
            title={`${choice} ${numeral(amountToSend).format('0.0a')} ${currency}`}
            icon={
              <Text
                style={{
                  fontFamily: 'icon-font',
                  color: '#ffffff',
                  fontSize: 18
                }}
              >
                {choice === 'Envoyer' ? <Text>&#xe904;</Text> : <Text>&#xe901;</Text>}
              </Text>
            }
            containerStyle={{
              shadowColor: 'rgba(0,0,0,0)',
              shadowOpacity: 0
            }}
          />
        </View>
        </View>
      </View>
      </Overlay>
    );
  }
}

const styles = StyleSheet.create({
  innnerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 50
  },
  iconStyle: {
    alignItems: 'flex-end',
    marginRight: 60,
    marginTop: 30,
    width
  },
  inputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    maxHeight: 50,
    width,
  },
  inputStyle: {
    alignSelf: 'stretch',
    borderBottomColor: 'rgba(0,0,0,0)',
    color: 'black',
    fontSize: 24,
    textAlign: 'center',
    width
  },
  buttonStyle: {
    backgroundColor: colors.green,
    borderRadius: 5,
    padding: 10,
    width: width - 50
  }
});