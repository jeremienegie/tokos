import React, {
  Component
}
from 'react';
import {
  View
}
from 'react-native';
import {
  ListItem
}
from 'react-native-elements';

import colors from '../assets/colors';

export default class ContactsList extends Component {
  handleClicked = (name, phoneNumbers, avatar) => {
    const phone = phoneNumbers[0].number;
    if (this.props.navigateToHistory) {
      const visibleTransHistory = () => {
        const user = {
          name,
          receiverPhone: phone,
          avatar,
          goBack: 'Profile'
        };
        this.props.visibleTransHistory(user);
        this.props.navigateToHistory('transactionHistory');
      };
      return visibleTransHistory();
    }
    return this.props.handleContactClicked(name, phone, avatar);
  }
  render() {
    const {
      contacts
    } = this.props;
    return (
      <View>
          {
            contacts[0].data.map((contact, index) => {
              const { name, phoneNumbers, avatar } = contact;
              const props = {
                roundAvatar: true,
                title: name,
                subtitle: phoneNumbers[0].number,
                subtitleStyle: {
                  color: colors.grey,
                  fontSize: 14
                },
                chevron: true,
                bottomDivider: true,
                onPress: () => this.handleClicked(name, phoneNumbers, avatar)
              };
              return (
                <View key={index}>
                  {
                    avatar
                    ? <ListItem
                      {...props}
                      avatar={{ source: { uri: avatar } }}
                    />
                  : <ListItem
                    {...props}
                    leftIcon={{
                      type: 'font-awesome',
                      name: 'user-circle',
                      size: 50,
                      color: colors.grey
                    }}
                  />
                  }
                </View>
              );
            })
          }
        </View>
    );
  }
}