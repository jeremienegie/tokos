import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet
}
from 'react-native';
import {
  LinearGradient
}
from 'expo';
import {
  Icon
}
from 'react-native-elements';
import {
  connect
}
from 'react-redux';

const {
  width
} = Dimensions.get('window');

class BankCards extends Component {
  state = {
    showDeleteCardBtn: false
  }
  deleteBtnVisvible = (cardNumber) => {
    this.props.showDeleteCardBtn({
      status: false
    });
    this.props.deleteBtnVisvible(cardNumber);
  }
  handleCardPress = () => {
    if (this.props.status) {
      return this.props.showDeleteCardBtn({
        status: false
      });
    }
    return this.props.handleCardPress();
  }
  hideAndSpace = (string, num) => {
    const stringSpilt = string.replace(/\s/g, '').match(/.{1,12}/g);
    const toBeReplaced = stringSpilt[0];
    const printHash = () => {
      const a = [];
      for (let i = 1; i <= num; i++) {
        a.push('*');
      }
      return a;
    };
    const hash = printHash(toBeReplaced).join('');
    stringSpilt[0] = hash;
    return stringSpilt.join('').match(/.{1,4}/g).join(' ');
  }
  renderCardLabel = (cardNumber) => {
    if (cardNumber.match(/.{1,2}/g)[0] === '51' || cardNumber.match(/.{1,2}/g)[0] === '55') {
      return (
        <Icon
          name="cc-mastercard"
          type="font-awesome"
          color="#ffffff"
          size={30}
        />
      );
    } else if (cardNumber.match(/.{1,1}/g)[0] === '4' || cardNumber.match(/.{1,1}/g)[0] === '1') {
      return (
        <Icon
          name="cc-visa"
          type="font-awesome"
          color="#ffffff"
          size={30}
        />
      );
    } else if (cardNumber.match(/.{1,2}/g)[0] === '34' || cardNumber.match(/.{1,2}/g)[0] === '37') {
      return (
        <Icon
          name="cc-amex"
          type="font-awesome"
          color="#ffffff"
          size={30}
        />
      );
    } else if (cardNumber.match(/.{1,2}/g)[0] === '35' || cardNumber.match(/.{1,2}/g)[0] === '62') {
      return (
        <Icon
          name="cc-jcb"
          type="font-awesome"
          color="#ffffff"
          size={30}
        />
      );
    } else if (cardNumber.match(/.{1,4}/g)[0] === '6011') {
      return (
        <Icon
          name="cc-discover"
          type="font-awesome"
          color="#ffffff"
          size={30}
        />
      );
    }
    return (
      <Icon
        name="credit-card"
        type="font-awesome"
        color="#ffffff"
        size={30}
      />
    );
  }
  renderCards = (cardNumber) => {
    if (cardNumber) {
      const {
        uiColor: {
          colorOne,
          colorTwo
        },
        expiration: {
          month,
          year
        }
      } = this.props;
      const {
        gradient,
        cardContainer,
        text
      } = styles;
      return (
        <View
          style={{ flex: 1, width }}
        >
          <LinearGradient
              colors={[colorOne, colorTwo]}
              style={gradient}
              start={[1, 0]}
              end={[0, 1]}
          >
              <TouchableWithoutFeedback
                onPress={this.handleCardPress}
                onLongPress={() => this.props.showDeleteCardBtn({ status: !this.props.status })}
              >
                <View style={cardContainer}>
                  <Text
                    style={text}
                  >
                    {this.hideAndSpace(cardNumber, 12)}
                  </Text>
                  <View style={{ width: '85%', marginTop: 10 }}>
                    <Text
                      style={[text, { fontSize: 16 }]}
                    >
                      {`${month}/${year}`}
                    </Text>
                  </View>
                  <View
                    style={{
                      position: 'absolute',
                      bottom: 15,
                      right: 20
                    }}
                  >
                    {this.renderCardLabel(cardNumber)}
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </LinearGradient>
          {this.props.status && <View
             style={{
               backgroundColor: '#ffffff',
               borderRadius: 100,
               position: 'absolute',
               top: 10,
               right: 10,
               zIndex: 9999
             }}
          >
            <Icon
              name="times-circle"
              size={30}
              color="#000"
              type="font-awesome"
              onPress={() => this.deleteBtnVisvible(cardNumber)}
            />
          </View>
          }
        </View>
      );
    }
    return <View />;
  }
  render() {
    const {
      cardNumber
    } = this.props;
    return this.renderCards(cardNumber);
  }
}

const styles = StyleSheet.create({
  gradient: {
    alignItems: 'center',
    borderRadius: 5,
    height: 180,
    margin: 20
  },
  cardContainer: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    padding: 15,
    paddingBottom: 35,
    height: '100%',
    width: '100%',
  },
  text: {
    color: '#ffffff',
    fontSize: 25,
    fontWeight: 'bold',
    marginHorizontal: 5
  }
});

const mapStateToProps = (state) => ({
  status: state.showDeleteCardBtn.status
});

export default connect(mapStateToProps)(BankCards);