import createMaterialBottomTabNavigator
from 'react-navigation-material-bottom-tabs/dist/navigators/createMaterialBottomTabNavigator';


import Home from '../screens/Home';
import Notifications from '../screens/Notifications';
import Wallet from '../screens/Wallet';
import Profile from '../screens/Profile';
import colors from '../assets/colors';

export default createMaterialBottomTabNavigator({
  Home,
  Notifications,
  Wallet,
  Profile
}, {
  activeTintColor: colors.green,
  barStyle: { paddingVertical: 6 }
});
