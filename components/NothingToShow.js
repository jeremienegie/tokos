import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
}
from 'react-native';
import {
  Icon
}
from 'react-native-elements';

import colors from '../assets/colors';

export default (props) => {
  const func = props.onPress ? props.onPress() : false;

  return (
    <TouchableOpacity
    style={styles.container}
    onPress={() => func}
    >
    <View
      style={styles.nothingToShow}
    >
      { props.icon
        ? <Icon
          name={props.icon}
          size={150}
          color={colors.grey}
          type="font-awesome"
        />
      : <View />
      }
      <Text
        style={{
          color: colors.grey,
          fontSize: 16,
          textAlign: 'center',
          marginHorizontal: 20,
          marginTop: 30,
        }}
      >
        {props.text}
      </Text>
    </View>
  </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  nothingToShow: {
    alignItems: 'center',
    flex: 1,
    paddingTop: '35%',
  }
});