import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

import ContactsList from './ContactsList';
import BackButton from '../components/BackButton';
import colors from '../assets/colors';

export default class Contact extends Component {
  render() {
    const {
      contacts,
      title,
      nothingToShow,
      overlay,
      handleContactClicked,
      dispatch,
      navigateToHistory,
      titleMarginTop
    } = this.props;
    const {
      nothingToShowStyle,
      nothingToShowTextStyle,
      titleStyle,
    } = styles;
    return (
      <View>
        <View>
          <BackButton dispatch={dispatch} to="transaction" />
          <View>
            <Text style={[titleStyle, { marginTop: titleMarginTop || 0 }]}>{title}</Text>
          </View>
          {
            contacts.length
            ? <View>
                <ContactsList
                  contacts={contacts}
                  overlay={overlay}
                  handleContactClicked={handleContactClicked}
                  navigateToHistory={navigateToHistory}
                  dispatch={dispatch}
                  visibleTransHistory={this.props.visibleTransHistory}
                />
              </View>
            : <View style={nothingToShowStyle}>
                <Text style={nothingToShowTextStyle}>{nothingToShow}</Text>
                <View style={{ marginVertical: 100 }}>
                  <Icon
                    name="person-add"
                    size={150}
                    color={colors.grey}
                  />
                </View>
              </View>
          }
        </View>
      </View>
    );
  }
}

const styles = {
  titleStyle: {
    fontWeight: 'bold',
    marginLeft: 20
  },
  nothingToShowStyle: {
    padding: 20
  },
  nothingToShowTextStyle: {
    color: colors.grey,
    textAlign: 'center',
    marginTop: 40
  }
};