import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import colors from '../assets/colors';

export default class Allow extends Component {
  render() {
    return (
      <View style={styles.notAllowedAccess}>
        <Text style={styles.textStyle}>{this.props.text}</Text>
        <Button
          large
          title="Allow"
          color='#ffffff'
          icon={<Icon name={this.props.icon} color="#ffffff" />}
          buttonStyle={{
            backgroundColor: colors.green,
            borderColor: 'transparent',
            borderWidth: 0,
            borderRadius: 5,
            padding: 10,
            width: 300
          }}
          onPress={() => this.props.callback()}
        />
      </View>
    );
  }
}

const styles = {
  notAllowedAccess: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 20
  },
  textStyle: {
    marginVertical: 20,
    textAlign: 'center'
  }
};