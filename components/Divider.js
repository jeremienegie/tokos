import React from 'react';
import { View } from 'react-native';

export default (props) => (
  <View
    style={{
      borderRightWidth: 1,
      borderColor: props.color,
      width: 1,
      backgroundColor: props.color,
      height: 30
    }}
  />
);