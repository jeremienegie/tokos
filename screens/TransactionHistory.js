import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  BackHandler,
  FlatList,
  Dimensions,
  TouchableOpacity
}
from 'react-native';
import {
  connect
}
from 'react-redux';
import {
  Header,
  Icon,
  Button
}
from 'react-native-elements';
import moment from 'moment';
import {
  NavigationActions
}
from 'react-navigation';
import {
  showMessage
}
from 'react-native-flash-message';

import colors from '../assets/colors';
import RenderAvatar from '../components/RenderAvatar';
import NothingToShow from '../components/NothingToShow';
import Keyboard from '../components/Keyboard';
import Divider from '../components/Divider';
import TransactionConfirm from '../components/TransactionConfirm';

import {
  overlay,
  startTransaction,
  amount,
  preSend,
  transactionChoice,
  visibleTransHistory
}
from '../actions';

const {
  width
} = Dimensions.get('window');

class TransactionHistory extends Component {
  static navigationOptions = () => ({
    swipeEnabled: false
  })

  state = {
    icon: '',
    value: <Text />
  }

  componentWillMount = () => {
    if (this.props.user.goBack === 'Home') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
  }
  componentWillUnmount = () => {
    if (this.props.user.goBack === 'Home') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
  }

  handleBackButton = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: this.props.user.goBack
    });
    this.props.navigation.dispatch(navigateAction);
    return true;
  };

  handleTransHisButtons = (choice) => {
    const {
      name,
      phone,
      avatar
    } = this.props.user;
    const {
      amountToSend,
      currency
    } = this.props.data;
    this.props.preSend({
      name,
      phone,
      avatar,
      amountToSend
    });
    this.props.transactionChoice(choice);
    if (this.toLowToSend()) {
      return showMessage({
        message: `You can not ${choice.toLowerCase()} ${amountToSend} ${currency}`,
        type: 'danger',
        icon: 'auto',
        hideStatusBar: true,
        duration: 3000
      });
    }
    this.props.overlay({
      status: true
    });
    return this.props.navigation.navigate('transactionHistory');
  }
  toLowToSend = () => {
    if (parseInt(this.props.data.amountToSend, 10) >= 100 &&
      this.props.data.currency === 'FC') {
      return false;
    } else if (parseInt(this.props.data.amountToSend, 10) >= 1 &&
      this.props.data.currency === '$') {
      return false;
    }
    return true;
  }

  handleTransButtonClick = (value, icon) => {
    const {
      overlayBtn,
      overlayBtnContainer
    } = styles;
    this.props.navigation.navigate('transaction', {
      transaction: <Keyboard
        dispatch={this.props.navigation.dispatch}
        to='transactionHistory'
      >
        <Button
          title={value}
          icon={icon}
          buttonStyle={overlayBtn}
          containerStyle={overlayBtnContainer}
          onPress={() => this.handleTransHisButtons(value)}
        />
      </Keyboard>
    });
  }
  handleProfileClick = () => {
    const {
      name,
      avatar,
      receiverPhone
    } = this.props.user;
    return this.props.navigation.navigate('QRCode', {
      name,
      avatar,
      phone: receiverPhone,
      to: 'transactionHistory'
    });
  }
  renderFooter = () => {
    const {
      footer,
      footerContainer,
      buttonContainer,
      buttonStyle,
      icon
    } = styles;
    return (
      <View style={footerContainer}>
      <View style={footer}>
        <View style={buttonContainer}>
        <TouchableOpacity
          style={[buttonStyle, {
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5
          }]}
          onPress={() => this.handleTransButtonClick('Envoyer',
            <Text style={[icon, { color: '#ffffff' }]}>&#xe904;</Text>
          )}
        >
          <Text style={{ fontFamily: 'icon-font', fontSize: 16 }}>&#xe904; Envoyer</Text>
        </TouchableOpacity>
        <Divider color="#000" />
        <TouchableOpacity
          style={[buttonStyle, {
            borderTopRightRadius: 5,
            borderBottomRightRadius: 5
          }]}
          onPress={() => this.handleTransButtonClick('Demander',
            <Text style={[icon, { color: '#ffffff' }]}>&#xe901;</Text>
          )}
        >
          <Text style={{ fontFamily: 'icon-font', fontSize: 16 }}>&#xe901; Demander</Text>
        </TouchableOpacity>
        </View>
        <RenderAvatar
          avatar={this.props.avatar}
          size={50}
          color={colors.grey}
          avatarSize="large"
        />
      </View>
    </View>
    );
  }
  render() {
    const {
      container,
      transactionContainer,
      choiceText
    } = styles;
    const {
      status,
      data,
      navigation: {
        dispatch
      }
    } = this.props;
    const startTrans = this.props.startTransaction;
    const visibleTransH = this.props.visibleTransHistory;
    const {
      name,
      avatar,
      receiverPhone
    } = this.props.user;
    const overlayClose = this.props.overlay;
    const LeftComponent = () => (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          flexDirection: 'row'
        }}
      >
      <View style={{ marginRight: 20 }}>
        <Icon
          name="arrow-left"
          color="#495057"
          type="font-awesome"
          onPress={this.handleBackButton}
        />
      </View>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row'
          }}
          onPress={this.handleProfileClick}
        >
          <RenderAvatar
            avatar={avatar}
            size={30}
            color={colors.grey}
            avatarSize="large"
          />
          <View style={{ marginLeft: 10 }}>
            <Text
              style={{
                color: '#000',
                textAlign: 'left'
              }}
            >
                {name}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
    const CenterComponent = () => (
      <View />
    );
    const RightComponent = () => (
      <View style={{ marginRight: 10 }}>
        <Icon
          name="ellipsis-v"
          color={colors.black}
          type="font-awesome"
        />
      </View>
    );
    const TransactionMessage = (props) => {
      const {
        choice,
        description,
        currency,
        sentAt
      } = props;
      const amountSent = props.amount;
      return (
        <View>
          <View style={transactionContainer}>
          <View style={{ alignSelf: 'flex-end' }}>
            {choice === 'Envoyer'
              ? <Text style={choiceText}>&#xe904;</Text>
              : <Text style={choiceText}>&#xe901;</Text>
            }
          </View>
          <View style={{ paddingHorizontal: 20 }}>
            <Text style={{ color: '#ffffff', paddingBottom: 5 }}>
              {`${amountSent} ${currency}`}
            </Text>
            <Text style={{ color: '#ffffff', marginBottom: 10 }}>{description}</Text>
          </View>
        </View>
        <View style={{ alignSelf: 'flex-end' }}>
          <Text
            style={{
              color: colors.grey,
              fontSize: 12,
              marginRight: 10
            }}
          >
            {moment(sentAt).fromNow()}
          </Text>
        </View>
        </View>
      );
    };
    const filterTransaction = () =>
      this.props.transactions.filter(({
        phone
      }) => phone === receiverPhone);
    return (
      <View style={container}>
        <TouchableOpacity
          onPress={this.handleProfileClick}
        >
          <Header
            placement='left'
            backgroundColor="#ffffff"
            leftComponent={<LeftComponent />}
            centerComponent={<CenterComponent />}
            rightComponent={<RightComponent />}
            headerStyle={{
              borderBottomWidth: 1,
              borderColor: '#000'
            }}
            outerContainerStyles={{
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
              height: 50
            }}
          />
        </TouchableOpacity>
        {filterTransaction().length ?
          <FlatList
            data={filterTransaction().reverse()}
            renderItem={({ item }) => (<TransactionMessage {...item} />)}
            keyExtractor={(item, index) => `${index}`}
            inverted
            ListHeaderComponent={
              <View
                style={{
                  backgroundColor: '#ffffff',
                  height: 75,
                  width: '100%'
                }}
              />
            }
          />
          :
          <NothingToShow
            text={`Ooops!!! ${name} and You haven't made any transaction to each other yet`}
            icon="envelope-open"
          />
        }
        {this.renderFooter()}
        <View style={{ flex: 1, position: 'absolute', top: StatusBar.currentHeight }}>
          <TransactionConfirm
            isVisible={status}
            overlayClose={overlayClose}
            data={data}
            dispatch={dispatch}
            startTransaction={startTrans}
            amountReset={amount}
            visibleTransHistory={visibleTransH}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight
  },
  transactionContainer: {
    alignSelf: 'flex-end',
    borderRadius: 10,
    borderBottomRightRadius: 0,
    backgroundColor: colors.green,
    margin: 10,
    marginBottom: 0,
    padding: 10,
    width: width - (width / 3)
  },
  footer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  footerContainer: {
    backgroundColor: '#ffffff',
    bottom: 0,
    paddingBottom: 10,
    paddingRight: 10,
    position: 'absolute',
    right: 0,
    width
  },
  buttonContainer: {
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderColor: 'rgba(0,0,0,0.1)',
    borderRadius: 10,
    borderWidth: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    marginRight: 10
  },
  buttonStyle: {
    backgroundColor: '#ffffff',
    borderColor: 'rgba(0,0,0,0.4)',
    padding: 15,
    paddingVertical: 20
  },
  buttonTitleStyle: {
    color: '#000000'
  },
  choiceText: {
    color: '#ffffff',
    fontSize: 16,
    fontFamily: 'icon-font',
    marginTop: 5,
    marginRight: 10
  },
  overlayBtnContainer: {
    height: '100%',
    shadowColor: 'transparent',
    shadowOpacity: 0,
    width: '100%'
  },
  overlayBtn: {
    backgroundColor: colors.green,
    borderRadius: 5,
    height: '100%',
    width: '100%'
  },
  icon: {
    color: colors.green,
    fontFamily: 'icon-font',
    fontSize: 24
  }
});

const mapStateToProps = (state) => {
  const {
    transactions,
    setCurrency,
    amountTyped
  } = state;

  const visibleTransH = state.visibleTransHistory;

  const {
    choice
  } = state.transactionChoice;
  const {
    currency
  } = setCurrency;
  const amountToSend = amountTyped.amountTyped;
  //ESLint is a bitch....

  return {
    transactions,
    avatar: undefined,
    user: visibleTransH,
    status: state.overlay.status,
    data: {
      ...visibleTransH,
      choice,
      currency,
      amountToSend,
    },
  };
};

export default connect(mapStateToProps, {
  overlay,
  startTransaction,
  amount,
  preSend,
  transactionChoice,
  visibleTransHistory
})(TransactionHistory);