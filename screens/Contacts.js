import React, {
  Component
}
from 'react';
import {
  View,
  StatusBar,
  Platform
}
from 'react-native';
import {
  AppLoading
}
from 'expo';
import {
  connect
}
from 'react-redux';

import Contacts from '../components/Contacts';
import TransactionComfirm from '../components/TransactionConfirm';
import Allow from '../components/Allow';
import {
  overlay,
  startTransaction,
  amount,
  storeContacts,
  startNotifications,
  preSend,
  visibleTransHistory
}
from '../actions';


class ShowContacts extends Component {
  static navigationOptions = () => ({
    swipeEnabled: false
  })

  handleContactClicked = (name, phone, avatar) => {
    const {
      amountToSend
    } = this.props.data;
    this.props.preSend({
      name,
      phone,
      avatar,
      amountToSend
    });
    this.props.overlay({
      status: true
    });
  }


  showContacts = () => {
    if (!this.props.isAllowed) {
      return (
        <AppLoading />
      );
    } else if (this.props.isAllowed !== 'granted') {
      return (
        <Allow
          text="Please allow Tokos to read your contacts"
          icon="contacts"
          callback={this.props.handleContacts()}
        />
      );
    }

    return (
      <View style={{ marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight }}>
        <Contacts
          contacts={this.props.contacts}
          title="Contacts on Tokos"
          nothingToShow="None of your contacts are on Tokos, Tell them about Tokos"
          dispatch={this.props.navigation.dispatch}
          overlay={this.props.overlay}
          handleContactClicked={this.handleContactClicked}
          titleMarginTop={50}
        />
        <TransactionComfirm
          isVisible={this.props.status}
          overlayClose={this.props.overlay}
          data={this.props.data}
          dispatch={this.props.navigation.dispatch}
          startTransaction={this.props.startTransaction}
          amountReset={this.props.amount}
          visibleTransHistory={this.props.visibleTransHistory}
        />
      </View>
    );
  }
  render() {
    return this.showContacts();
  }
}

const mapStateToProps = (state) => {
  const {
    name,
    phone,
    avatar
  } = state.preSend;
  const {
    choice
  } = state.transactionChoice;
  const {
    currency
  } = state.setCurrency;
  const {
    amountTyped
  } = state.amountTyped;
  return {
    status: state.overlay.status,
    isAllowed: state.setContacts.value,
    contacts: state.setContacts.contacts,
    data: {
      name,
      amountToSend: amountTyped,
      phone,
      avatar,
      choice,
      currency
    }
  };
};

export default connect(mapStateToProps, {
  overlay,
  startTransaction,
  amount,
  storeContacts,
  startNotifications,
  preSend,
  visibleTransHistory
})(ShowContacts);