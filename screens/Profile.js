import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity
}
from 'react-native';
import {
  Icon
}
from 'react-native-elements';
import {
  connect
}
from 'react-redux';

import RenderAvatar from '../components/RenderAvatar';
import Contacts from '../components/Contacts';
import colors from '../assets/colors';
import {
  visibleTransHistory
}
from '../actions';

class Profile extends Component {
  static navigationOptions = {
    tabBarColor: 'white',
    tabBarIcon: ({
      tintColor
    }) => <RenderAvatar
      avatar='https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg'
      size={20}
      color={tintColor}
      avatarSize="large"
    />
  }
  render() {
    const {
      container,
      profileContainer,
      profile
    } = styles;
    return (
      <View style={container}>
        <TouchableOpacity
          style={profileContainer}
          onPress={() => this.props.navigation.navigate('QRCode', {
            name: 'Michael Doe',
            phone: '+243904200981',
            to: 'Profile',
            avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg'
          })}
        >
          <View style={profile}>
            <RenderAvatar
              avatar='https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg'
              size={60}
              color={colors.grey}
              avatarSize="large"
            />
          <Text style={{ paddingLeft: 10, fontSize: 20 }}>Michael Doe</Text>
          </View>
          <Icon
            name="cog"
            size={20}
            color={colors.green}
            type="font-awesome"
          />
        </TouchableOpacity>
        <View
          style={{
            backgroundColor: colors.grey,
            borderColor: colors.grey,
            borderBottomWidth: 1,
            marginVertical: 20,
            marginHorizontal: 40
          }}
        />
        <Contacts
          contacts={this.props.contacts}
          title="Contact on Tokos"
          nothingToShow="None of your contacts are on Tokos, Tell them about Tokos"
          navigateToHistory={this.props.navigation.navigate}
          visibleTransHistory={this.props.visibleTransHistory}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    flex: 1,
    paddingTop: StatusBar.currentHeight
  },
  profileContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 30,
    paddingBottom: 0
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
});

const mapStateToProps = ({
  setContacts
}) => ({
  value: setContacts.value,
  contacts: setContacts.contacts
});

export default connect(mapStateToProps, {
  visibleTransHistory
})(Profile);