import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  FlatList
}
from 'react-native';
import {
  connect
}
from 'react-redux';
import {
  ListItem
}
from 'react-native-elements';
import moment from 'moment';
import RenderAvatar from '../components/RenderAvatar';
import colors from '../assets/colors';

import {
  startNotifications,
  visibleTransHistory
}
from '../actions';
import NothingToShow from '../components/NothingToShow';


class Notifcations extends Component {
  static navigationOptions = {
    tabBarColor: 'white',
    tabBarIcon: ({
      tintColor
    }) => <Text
    style={{
      color: tintColor,
      fontFamily: 'icon-font',
      fontSize: 20
    }}
    >&#xe906;</Text>,
  };

  state = {
    refreshing: false
  }

  componentWillMount = () => {
    this.props.startNotifications();
  }
  handleStartNotifications = () => {
    this.setState({
      refreshing: false
    }, () => {
      this.props.startNotifications();
    });
  }
  handleRefresh = () => {
    this.setState({
      refreshing: true
    }, () => {
      this.handleStartNotifications();
    });
  }
  handleContactClick = (phone, avatar) => {
    const user = {
      name: this.rtName(phone),
      avatar,
      receiverPhone: phone,
      goBack: 'Notifications'
    };
    this.props.visibleTransHistory(user);
    this.props.navigation.navigate('transactionHistory');
  }
  rtName = (phone) => {
    const name = this.props.contacts[0].data.map(contact => {
      const phoneNumOnPhone = contact.phoneNumbers[0].number;
      if (phoneNumOnPhone !== phone) {
        return '';
      }
      return contact.name;
    });
    return name.join('');
  }
  renderThisWeek = (TransactionNotif) => {
    if (this.props.thisWeek.length) {
      return (
        <View>
        <View style={{ marginVertical: 20, marginLeft: 20 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>This week</Text>
        </View>
        <FlatList
          data={this.props.thisWeek.reverse()}
          renderItem={({ item }) => (<TransactionNotif {...item} />)}
          keyExtractor={(item, index) => `${index}`}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
        />
      </View>
      );
    }
  }
  renderLastWeek = (TransactionNotif) => {
    if (this.props.lastWeek.length) {
      return (
        <View>
        <View style={{ marginVertical: 20, marginLeft: 20 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Last week</Text>
        </View>
        <FlatList
          data={this.props.lastWeek.reverse()}
          renderItem={({ item }) => (<TransactionNotif {...item} />)}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
      );
    }
  }
  renderOlder = (TransactionNotif) => {
    if (this.props.older.length) {
      return (
        <View>
          <View style={{ marginVertical: 20, marginLeft: 20 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Older</Text>
          </View>
          <FlatList
            data={this.props.older.reverse()}
            renderItem={({ item }) => (<TransactionNotif {...item} />)}
            keyExtractor={(item, index) => `${index}`}
          />
        </View>
      );
    }
  }
  render() {
    const {
      container
    } = styles;
    const TransactionNotif = (props) => {
      const {
        avatar,
        phone,
        description,
        amount,
        sentAt,
        currency
      } = props;
      return (
        <View>
        <ListItem
          title={this.rtName(phone)}
          subtitle={description || moment(sentAt).fromNow()}
          rightElement={<Text>{`- ${amount} ${currency}`}</Text>}
          leftAvatar={
            <RenderAvatar
              avatarSize="large"
              size={50}
              color={colors.grey}
              avatar={avatar}
            />
          }
          bottomDivider
          onPress={() => this.handleContactClick(phone, avatar)}
        />
        </View>
      );
    };

    return (
      <View style={container}>
        {!this.props.thisWeek.length && !this.props.lastWeek.length && !this.props.older.length
          ? <NothingToShow
            text="Ooops!! you don't have any notifications yet"
            icon="bell"
            onPress={this.props.startNotifications}
          />
          : <View>
            {this.renderThisWeek(TransactionNotif)}
            {this.renderLastWeek(TransactionNotif)}
            {this.renderOlder(TransactionNotif)}
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: StatusBar.currentHeight
  }
});

const mapStateToProps = ({
  thisWeek,
  lastWeek,
  older,
  setContacts
}) => ({
  thisWeek,
  lastWeek,
  older,
  contacts: setContacts.contacts
});

export default connect(mapStateToProps, {
  startNotifications,
  visibleTransHistory
})(Notifcations);