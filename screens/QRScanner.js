import React, {
  Component
}
from 'react';
import {
  Permissions,
  BarCodeScanner
}
from 'expo';
import {
  View,
  StyleSheet
}
from 'react-native';

import Allow from '../components/Allow';

export default class QRScanner extends Component {
  static navigationOptions = () => ({
    swipeEnabled: false
  })

  state = {
    hasCameraPermission: null
  }

  async componentWillMount() {
    this.handlePermission();
  }

  handlePermission = async () => {
    const {
      status
    } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted'
    });
  }

  handleBarCodeRead = (data) => {
    console.log(data);
  }

  render() {
    const {
      hasCameraPermission
    } = this.state;
    if (hasCameraPermission === null) {
      return (
        <Allow
          text="Please allow Tokos to use camera"
          icon="photo-camera"
          callback={this.handlePermission}
        />
      );
    } else if (hasCameraPermission === false) {
      return (
        <Allow
          text="Tokos need you to allow the use of your phone's camera"
          icon="photo-camera"
          callback={this.handlePermission}
        />
      );
    }
    return (
      <View>
				<BarCodeScanner
					onBarCodeRead={this.handleBarCodeRead}
					style={StyleSheet.absoluteFill}
					barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
				/>
			</View>
    );
  }
}