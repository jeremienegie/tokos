import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar
}
from 'react-native';
import QRCode from 'react-native-qrcode-svg';

import BackButton from '../components/BackButton';
import RenderAvatar from '../components/RenderAvatar';
import colors from '../assets/colors';

export default class QRCodeScreen extends Component {
  render() {
    const {
      container,
      textStyle,
      avatarStyle
    } = styles;
    const {
      navigation: {
        getParam
      }
    } = this.props;
    const phone = getParam('phone');
    const name = getParam('name');
    const avatar = getParam('avatar');
    const to = getParam('to');
    return (
      <View style={container}>
        <BackButton
          to={to}
          dispatch={this.props.navigation.dispatch}
        />
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 120
        }}
      >
        <View>
          <Text style={textStyle}>
            {name}
          </Text>
          <QRCode
            value={phone}
            size={250}
            color={colors.grey}
          />
          <View
            style={[avatarStyle, { borderWidth: avatar ? 6 : 0 }]}
          >
            <RenderAvatar
              avatar={avatar}
              size={50}
              color={colors.grey}
              avatarSize="large"
            />
          </View>
        </View>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBar.currentHeight
  },
  textStyle: {
    color: colors.grey,
    fontSize: 16,
    paddingVertical: 10
  },
  avatarStyle: {
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderRadius: 100,
    position: 'absolute',
    bottom: -15,
    right: -15
  }
});