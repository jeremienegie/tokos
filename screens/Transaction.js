import React, {
  Component
}
from 'react';
import {
  View
}
from 'react-native';

export default class Transaction extends Component {
  static navigationOptions = {
    swipeEnabled: false
  }

  render() {
    const transaction = this.props.navigation.getParam('transaction');
    return React.createElement(View, {
      style: {
        flex: 1
      }
    }, transaction);
  }
}