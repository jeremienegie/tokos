import React, {
  Component
}
from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  KeyboardAvoidingView
}
from 'react-native';
import {
  Input,
  Icon
}
from 'react-native-elements';
import moment from 'moment';
import ModalDropdown from 'react-native-modal-dropdown';
import {
  SecureStore
}
from 'expo';
import {
  showMessage
}
from 'react-native-flash-message';

import BackButton from '../components/BackButton';
import colors from '../assets/colors';
import uiGradient from '../assets/uiGradient';

export default class AddBankCard extends Component {
  state = {
    cardNumber: '',
    cvv: '',
    month: '',
    year: '',
    parseOldCard: [],
    colorTextOne: colors.grey,
    colorTextTwo: colors.grey
  }
  componentWillMount = async () => {
    const getAllOldAddedCard = await SecureStore.getItemAsync('cards');
    const parseOldCard = JSON.parse(getAllOldAddedCard);
    if (getAllOldAddedCard) {
      this.setState({
        parseOldCard
      });
    }
  }
  hadleCardNumberInput = (value) => {
    const strWithoutSpace = value.replace(/\s/g, '').replace(/\D/g, '');
    if (!isNaN(strWithoutSpace) && strWithoutSpace.length !== 0) {
      const str = strWithoutSpace.match(/.{1,16}/g)[0];
      const cardNumber = str.match(/.{1,4}/g).join(' ');
      return this.setState({
        cardNumber
      });
    }
    return this.setState({
      cardNumber: ''
    });
  }
  handleCVV = (value) => {
    const strWithoutSpace = value.replace(/\s/g, '').replace(/\D/g, '');
    if (!isNaN(strWithoutSpace) && strWithoutSpace.length !== 0) {
      const cvv = strWithoutSpace.match(/.{1,3}/g)[0];
      return this.setState({
        cvv
      });
    }
    return this.setState({
      cvv: ''
    });
  }
  arrayOfNum = (start, numOfTimes) => {
    const num = Number(String(start).replace('20', ''));
    const a = [];
    for (let i = num; i <= numOfTimes; i++) {
      a.push(i);
    }
    return a;
  }
  // Luhn algorithm validator, by Avraham Plotnitzky. (aviplot at gmail)
  luhnCheckFast2 = (luhn) => {
    let ca;
    let sum = 0;
    let mul = 1;
    let len = luhn.length;
    while (len--) {
      ca = parseInt(luhn.charAt(len), 10) * mul;
      sum += ca - ((ca > 9) * 9); // sum += ca - (-(ca>9))|9
      // 1 <--> 2 toggle.
      mul ^= 3; // (mul = 3 - mul);
    }
    return (sum % 10 === 0) && (sum > 0);
  };
  handleAddBankCard = async () => {
    const {
      cardNumber,
      cvv,
      month,
      year,
    } = this.state;
    if (!cardNumber || !cvv || !month || !year) {
      return showMessage({
        message: 'Please, make sure all the fields are filled',
        type: 'warning',
        icon: 'auto',
        hideStatusBar: true,
        duration: 3000
      });
    }
    const luhnCheck = this.luhnCheckFast2(cardNumber.replace(/\s/g, ''));
    if (luhnCheck) {
      const allCards = [...this.state.parseOldCard, {
        cardNumber,
        cvv,
        expiration: {
          month,
          year
        },
        uiColor: {
          ...uiGradient[Math.floor(Math.random() * uiGradient.length)]
        }
      }];

      const handleAddCard = this.props.navigation.getParam('handleAddCard');
      const stringifyAllCards = JSON.stringify(allCards);

      const err = await SecureStore.setItemAsync('cards', stringifyAllCards);
      if (err) {
        return showMessage({
          message: 'Something went wrong, please try again later',
          type: 'warning',
          icon: 'auto',
          hideStatusBar: true,
          duration: 3000
        });
      }
      handleAddCard(allCards);
      showMessage({
        message: 'Your card has been saved :-)',
        type: 'success',
        icon: 'auto',
        hideStatusBar: true,
        duration: 3000
      });
      return this.props.navigation.navigate('Wallet');
    }

    return showMessage({
      message: 'The card number you provided is invalide',
      type: 'danger',
      icon: 'auto',
      hideStatusBar: true,
      duration: 3000
    });
  }
  render() {
    const {
      container,
      titleStyle,
      containerStyle,
      inputContainerStyle,
      dropdownStyle,
      dropdownTextStyle,
      dropdownDefaultValueTextStyle,
      dropDownStyle,
      labelStyle,
      buttonStyle
    } = styles;
    return (
      <View style={container}>
        <View style={{ flex: 1, alignSelf: 'flex-start' }}>
          <BackButton
            to='Wallet'
            dispatch={this.props.navigation.dispatch}
            customStyle={{ left: 0 }}
          />
        </View>
        <Text style={titleStyle}>Add a bank card</Text>
        <View>
          <Input
            placeholder="1111 2222 3333 4444"
            label="Card number"
            labelStyle={labelStyle}
            keyboardType="phone-pad"
            value={this.state.cardNumber}
            onChangeText={(cardNumber) => this.hadleCardNumberInput(cardNumber)}
            leftIcon={{
              name: 'credit-card',
              type: 'font-awesome'
            }}
            containerStyle={containerStyle}
            inputContainerStyle={inputContainerStyle}
          />
        </View>
        <View
          style={containerStyle}
        >
          <Text style={labelStyle}>Expiration date</Text>
          <View
            style={[inputContainerStyle, {
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 10,
              paddingLeft: 15
            }]}
          >
            <View>
              <Icon
                name="calendar"
                type="font-awesome"
              />
            </View>
            <View
              style={{
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'center'
              }}
            >
              <ModalDropdown
                options={this.arrayOfNum(moment().month() + 1, 12)}
                defaultValue="Month"
                style={dropdownStyle}
                dropdownTextStyle={dropdownTextStyle}
                textStyle={[dropdownDefaultValueTextStyle, { color: this.state.colorTextOne, }]}
                dropdownStyle={dropDownStyle}
                onSelect={(index, value) => this.setState({ month: value, colorTextOne: '#000' })}
              />
              <ModalDropdown
                options={this.arrayOfNum(moment().year(), 50)}
                defaultValue="Year"
                style={dropdownStyle}
                dropdownTextStyle={dropdownTextStyle}
                textStyle={[dropdownDefaultValueTextStyle, { color: this.state.colorTextTwo, }]}
                dropdownStyle={dropDownStyle}
                onSelect={(index, value) => this.setState({ year: value, colorTextTwo: '#000' })}
              />
            </View>
          </View>
        </View>
        <KeyboardAvoidingView
          behavior="padding"
        >
          <Input
            label="Security code"
            labelStyle={labelStyle}
            placeholder="123"
            keyboardType="phone-pad"
            value={this.state.cvv}
            onChangeText={(cvv) => this.handleCVV(cvv)}
            leftIcon={{
              name: 'lock',
              type: 'font-awesome'
            }}
            containerStyle={containerStyle}
            inputContainerStyle={inputContainerStyle}
          />
        </KeyboardAvoidingView>
        <TouchableOpacity
          style={containerStyle}
          onPress={this.handleAddBankCard}
        >
          <View style={[inputContainerStyle, buttonStyle]}>
            <Icon
              name="save"
              type="font-awesome"
              iconStyle={{ color: '#ffffff', marginRight: 5 }}
            />
            <Text
              style={{
                color: '#ffffff',
                fontSize: 16,
                marginLeft: 5,
                fontWeight: 'bold'
              }}
            >
              Save
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
    padding: 20,
    paddingTop: StatusBar.currentHeight
  },
  titleStyle: {
    alignSelf: 'flex-start',
    backgroundColor: '#ffffff',
    color: colors.green,
    fontSize: 36,
    marginBottom: 20
  },
  containerStyle: {
    width: '100%'
  },
  inputContainerStyle: {
    borderColor: colors.grey,
    borderRadius: 5,
    borderWidth: 2,
    height: 60,
    width: '100%'
  },
  dropdownStyle: {
    paddingHorizontal: 20
  },
  dropDownStyle: {
    paddingHorizontal: 20
  },
  dropdownTextStyle: {
    color: 'black',
    fontSize: 24
  },
  dropdownDefaultValueTextStyle: {
    fontSize: 18
  },
  labelStyle: {
    color: colors.grey,
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 10
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: colors.green,
    borderWidth: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20
  },
  iconStyle: {
    color: '#ffffff'
  }
});