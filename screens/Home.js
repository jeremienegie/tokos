import React, {
  Component
}
from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity
}
from 'react-native';

import colors from '../assets/colors';
import MainTransKeyboard from '../components/MainTransKeyboard';

const tokosLogo = require('../assets/images/tokos.png');

export default class Home extends Component {
  static navigationOptions = {
    tabBarColor: 'white',
    tabBarIcon: ({
      tintColor
    }) => (<Text
    style={{
      color: tintColor,
      fontFamily: 'icon-font',
      fontSize: 25
    }}
    >&#xe908;</Text>)
  };

  handleButtonClick = () => {
    const {
      navigation: {
        dispatch,
        navigate
      }
    } = this.props;
    this.props.navigation.navigate('transaction', {
      transaction: <MainTransKeyboard
        dispatch={dispatch}
        navigate={navigate}
      />
    });
  }

  render() {
    const {
      container,
      button,
      image
    } = styles;
    return (
      <View style={container}>
        <TouchableOpacity
          style={button}
          onPress={() => this.handleButtonClick()}
        >
          <Image style={image} source={tokosLogo} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.green,
    flex: 1,
    justifyContent: 'center'
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: 'rgba(0,0,0,0.1)',
    borderWidth: 8,
    borderRadius: 100,
    height: 110,
    justifyContent: 'center',
    width: 110
  },
  image: {
    height: 50,
    width: 50
  }
});